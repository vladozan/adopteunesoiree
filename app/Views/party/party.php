<?php $this->layout('layout', ['title' => 'Soirée']) ?>

<?php $this->start('main_content') ?>
<div class="panel-body">

        <div class="informations col-md-4">
            <h3><a href="<?= $this->url('profil_profil', ['profil' => $donnees["membre_id"]]); ?>">De <?= $donnees["pseudo"]; ?></a></h3>
            <p>Contact:
                <?= $donnees["mail"]; ?>
            </p>
        </div>
        <div class="informations col-md-offset-2 col-md-6">
            <h3><?= $donnees["adresse_soiree"]." ".$donnees["codePostale"]." ".$donnees["ville_soiree"];?></h3>
            <p>Date :
                <?php echo "Le ".date( "d/m/Y \à H:m:s", strtotime($donnees["date_soiree"])); ?>
            </p>
        </div>
        <div class="row">
            <div class="description col-md-12">
                <h3 class="hdp">
                    Soirée : <?= $donnees["type_soiree"]; ?>
                </h3>
                <h3>Description</h3>
                <p class="messagerie-text ">
                    <?= $donnees["info"]; ?><br>
                </p>
                <?php if(isset($_SESSION["login"]) AND $donnees["id_hote"] != $_SESSION["login"]["id"]){ ?>
                 <form action="" method="POST">
                    <input type="submit" value="S'incruster" class="col-sm-offset-10 btn btn-primary">
                    <input type="hidden" name="id_hote" value="<?= $donnees["id_hote"] ?>">
                    <input type="hidden" name="id_invite" value="<?= $_SESSION["login"]["id"] ?>">
                    <input type="hidden" name="id_soiree" value="<?= $donnees["id"] ?>">
                    <input type="hidden" name="name_soiree" value="<?= $donnees["type_soiree"] ?>">
                    <?php if(isset($error)) { ?>
                    <div class="alert alert-warning alert-dismissable">
                        
                         <?= $error; ?>
                    </div>
                      <?php  } ?>
                    <p class="success help-block"></p>
                      <?php if(isset($success)) { ?>
                        <div class="alert alert-success alert-dismissable">
                             <?= $success; ?>
                        </div>
                      <?php  } ?>
                </form>
                <?php  } ?>
            </div>
        </div>
    </div>
    <?php if(isset($_SESSION["login"]) AND $donnees["id_hote"] == $_SESSION["login"]["id"]){ ?>
   <div class="party_member">
        <form action="" method="POST" class="col-md-4">
            <div class="waiting">
               <h4 class="center bleu">Ils veulent s'incruster</h4>
                <div class="member_list form-control">
                    <ul class=""><?php
                        foreach($waiting as $wait)
                        {   ?>             
                            <li><input type="checkbox" name="<?= $wait['membre_id'] ?>" value="<?= $wait["validation"] ?>"> <a href="<?= $this->url("profil_profil", ["profil" => $wait["membre_id"]]) ?>"><?= $wait["pseudo"] ?></a></li><?php
                        } ?>
                    </ul>
                </div>
                <div class="form-group">
                    <input type="submit" class="form-control btn btn-info" name="validate" value="Inviter">
                </div>
            </div>
        </form>
        <form action="" method="POST" class="col-md-offset-1 col-md-4">
            <div class="validate_member">
                <h4 class="center rose">Ils sont invités</h4>
                <div class="member_list form-control">
                    <ul><?php
                        foreach($validate as $valid)
                        {   ?>             
                            <li><input type="checkbox" class="checkbox_value" name="<?= $valid['membre_id'] ?>" value="<?= $valid["validation"] ?>"> <a href="<?= $this->url("profil_profil", ["profil" => $valid["membre_id"]]) ?>"><?= $valid["pseudo"] ?></a></li><?php
                        } ?>
                    </ul>
                </div>
                <div class="form-group">
                    <a href="#" data-width="500" data-rel="popup1" class="form-control btn btn-danger poplight">Désinviter</a>
                </div>
            </div>
            <div id="popup1" class="popup_block">
                <h3>Etes vous sur de vouloir refuser Cette personne</h3>
               <div class="form-group col-md-offset-3 col-md-4">
                   <input type="submit" class="btn btn-success" name="oui" value="Oui">
               </div>
                <div class="form-group col-md-4">
                    <button class="btn btn-danger">Non</button>
               </div>
            </div>
        </form>
    </div>

<?php }
 
$this->stop('main_content') ?>