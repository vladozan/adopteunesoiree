<?php $this->layout('layout', ['title' => 'Recherche de soiree']) ?>

<?php $this->start('main_content') ?>
    <div class="row">
        <div id="top" class="col-md-8 col-md-push-2 ">
          <h3 class="hdp rose">Choisissez la <span class="bleu">soirée qui</span> vous intéresse.</h3><br>
          <a href="#recherche" class="col-xs-offset-1 col-sm-offset-8 hidden-md hidden-lg">Faites une recherche avancée.</a>
          <div class="panel-group" id="accordion">
           <?php 
            $i = 1;
    // Affichage des soirées.
            foreach($requete as $donnees)
            { ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                       <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $i; ?>">
                        <h4 class="panel-title collapse_party"><?= $donnees["type_soiree"] ?></h4></a>
                    </div>
                    <div id="collapse<?= $i; ?>" class="panel-collapse collapse <?php if(isset($collapse["i"]) AND $collapse["i"] == $i){ echo " in "; } elseif(!isset($collapse["i"]) AND $i == 1) { echo "in"; } ?>">
                        <div class="panel-body">
                            
                            <div class="informations col-md-4">
                                <h3><a href="<?= $this->url('profil_profil', ['profil' => $donnees["membre_id"]]); ?>"><?= $donnees["pseudo"]; ?></a></h3>
                                <p>Contact:
                                    <?= $donnees["mail"]; ?>
                                </p>
                            </div>
                            <div class="informations col-md-offset-2 col-md-6">
                                <h3><?= $donnees["adresse_soiree"]." ".$donnees["codePostale"]." ".$donnees["ville_soiree"];?></h3>
                                <p>Date :
                                    <?php echo "Le ".date( "d-m-Y", strtotime($donnees["date_soiree"])); ?>
                                </p>
                            </div>
                            <div class="row">
                                <div class="description col-md-12">
                                    <h3>Description</h3>
                                    <span class="guillemet-left fa fa-quote-left fa-2x"></span>
                                    <p class="description-text">
                                        <?php
                                        
                                        $char = (strlen($donnees["info"]) > 300)? substr($donnees["info"], 0, 300)."..." : $donnees["info"];
                                        echo $char;?>
                                    </p>
                                    <span class="guillemet-right fa fa-quote-right fa-2x"></span>
                                </div>
                            </div>
                            <a class="guillemet-right" href="<?= $this->url('party_party', ['soiree' => $donnees['id']]) ?>">Vous êtes intéressé par cette soirée ?</a>
                        </div>
                    </div>
                </div>
            <?php $i++; }
        // pagination
            $j = 1;
            $precedent = (isset($get_page))? $get_page -1 : 0;
            $suivant = (isset($get_page))? $get_page +1 : 1;
              ?>
            <ul class="pager">
                <?php if($precedent > 0) { ?><li><a href="<?php echo $this->url("party_search_party", ["page" => $precedent]); ?>">Précédent</a></li><?php } 
                if($page_total > 0){ ?>
                <select id="select">
                <?php while($j <= $page_total){ ?>
                    <option class="col-md-1" <?php if($j == $get_page) echo "selected"; ?> value="<?= $j; ?>"><?= $j; ?></option>
                <?php $j++; } ?>
                </select>
                <?php }
                if($suivant <= $page_total) { ?><li><a href="<?php echo $this->url("party_search_party", ["page" => $suivant]); ?>">Suivant</a></li><?php } ?>
            </ul>
          </div> 
        </div>
        <!--	Formulaire de filtre de recherche -->

        <form action="<?php echo $this->url("party_search_party", ["page" => 1]) ?>" method="POST" id="recherche" class="form_soiree col-md-2 col-md-pull-8" role="form">

            <div class="form-group">
                <label for="type_soiree">Recherche plat: </label>
                <br>
                <input type="text" name="type_soiree" id="type_soiree" class="form-control" value="" placeholder="Saisissez votre plat">

            </div>

            <div class="form-group">
                <label for="date_soiree">Date et heure: </label>
                <br>
                <input type="datetime" name="date_soiree" id="date_soiree" class="form-control" placeholder="YYYY-MM-DD H:m:s">
            </div>


            <div class="form-group">
                <label for="adresse_soiree">Adresse: </label>
                <br>
                <input type="text" id="adresse_soiree" name="adresse_soiree" class="form-control">
            </div>

            <div class="form-group">
                <label for="ville">Ville: </label>
                <br>
                <input type="text" id="ville" name="ville_soiree" class="form-control">
            </div>

            <div class="form-group">
                <label for="codePostale">Code Postal: </label>
                <br>
                <input type="text" id="codePostale" name="codePostale" class="form-control">
            </div>

            <div class="form-group">
                <label class="" for="nombre_perso_max" type="">Max. personnes:</label>
                <br>
                <input type="number" id="nombre_perso_max" name="nombre_perso_max" class="form-control">

            </div>


            <button type="submit" class="form-control btn btn-primary">Rechercher</button>
        </form>
    </div>
<?php $this->stop('main_content') ?>
