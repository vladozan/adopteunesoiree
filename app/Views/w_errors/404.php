<?php $this->layout('layout', ['title' => 'Perdu ?']) ?>

<?php $this->start('main_content'); ?>
    <div class="row erreur_404">
       <div class="col-xs-12 erreur_centre">
          <img class="col-xs-offset-3 col-xs-6 img-reponsive" src="<?= $this->assetUrl('image/chute_serveur_blanc.svg') ?>" alt=""><br>
           <b><h1 class="col-xs-12 text-center" style="color:#eb89af;">Vous<span style="color:#84c1fa;"> êtes </span>perdu ?</h1></b>
          <p class="col-xs-12 text-center text_erreur">Retourner sur <a href="<?= $this->url("default_home") ?>">l'acceuil !</a></p>
        </div>
    </div>
<?php $this->stop('main_content'); ?>
