<!DOCTYPE html >
<html xmlns:og="http://ogp.me/ns#" lang="fr">
    <head>
        <title><?= $this->e($title) ?></title>
        <meta charset="UTF-8">
        <!--        Open Graph-->
        <meta property="og:title" content="Adopte une soiree.com" />
        <meta property="og:type" content="article"/>
        <meta property="og:description" content="Site permettant au personne seule en soirée ou en week end de trouver des personnes dans le même cas et de combler cette solitude" />
        <meta property="og:url" content="http://jinou-dev.fr" />
        <meta property="og:image" content="http://jinou-dev.fr/assets/image/logo-haut-g-openg.jpg" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?= $this->assetUrl('css/normalize.css') ?>">
        <link rel="stylesheet" href="<?= $this->assetUrl('css/styles.css') ?>">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= $this->assetUrl('css/font-awesome.min.css') ?>">
        <script src="<?= $this->assetUrl('js/jquery-3.1.1.min.js') ?>"></script>
        <script src="<?= $this->assetUrl('js/script.js') ?>"></script>

    </head>
    <body>
        <header class="container-fluid header">
            <div class="row">
                <img id="logo" src="<?= $this->assetUrl('image/logo-haut-g.svg') ?>" class="hidden-sm hidden-xs col-lg-1 hidden-md">
                <a href="<?= $this->url('default_home') ?>"><img id="logo" class="img-responsive col-md-3 col-lg-offset-1 col-md-offset-0 col-sm-offset-3 col-sm-5 col-xs-9" src="<?= $this->assetUrl('image/image_AUS.png') ?>" alt="Adote une soirée.com"></a>
                <?php if(isset($_SESSION["login"])){ ?>
                    <div class="my_drop btn-group col-md-offset-5 col-xs-offset-1 col-sm-offset-2">
                        <a class="droplink dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php if($_SESSION["messagerie"]["nb_message"] > 0) { ?> <span class="glyphicon glyphicon-envelope rose"></span> <?php } ?> Mon compte <span class="caret"></span>
                    </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= $this->url("setting_setting") ?>"><span class="glyphicon glyphicon-cog"></span> Paramètre</a></li>
                            <li><a href="<?= $this->url("history_party", ["page" => 1, "tab" => "crees"]); ?>"><span class="glyphicon glyphicon-folder-open"></span> Historique</a></li>
                            <li><a href="<?= $this->url("mailbox_mailbox", ["page" => 1]) ?>"><span class="glyphicon glyphicon-envelope"></span> Messagerie <?php if($_SESSION["messagerie"]["nb_message"]) { ?> <span class="badge badge-success"><?= $_SESSION["messagerie"]["nb_message"] ?></span><?php } ?></a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?= $this->url("register_logout") ?>"><span class="glyphicon glyphicon-off"></span> Déconnexion</a></li>
                        </ul>
                    </div>
                    <?php }
            else{ ?>
                        <form class="form-inline" action="<?= $this->url("register_login") ?>" method="POST">
                            <div class="form-group col-md-offset-1 col-sm-offset-3">
                                <input class="form-control" type="text" name="pseudo" placeholder="Pseudo">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="password" name="password" placeholder="Mot de passe">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="submit" value="Connexion">
                            </div>
                            <div class=" form-group checkbox col-md-offset-1 col-sm-offset-3">
                                <label class="rose">
                                    <input type="checkbox" name="cookie" value="1"><b>Connexion automatique</b>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?= $this->url('register_register') ?>">Pas encore inscrit ?</a>
                                    <p class="help-block">En cochant cette case vous acceptez l'utilisation des cookies.</p>
                                </label>
                                <?php  if(isset($error)) echo $error; ?>
                            </div>
                    </form>
            <?php   } ?>
                </div>
        </header>
        <?= $this->section('menu_setting') ?>
        <div class="container corps">
            <section>
                <?= $this->section('main_content') ?>
            </section>

            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        </div>
	    <footer class="container-fluid">
            <div class="row">
                <div class="foot">
                    <div class="container footer">
                        <p class=" hdp rose">Adopte <span class="bleu">une soirée</span>.com</p>
                        <ul class="list-inline  footer_menu">
                            <li>
                                <a href=""><img class="img-responsive fb" src="<?= $this->assetUrl('image/logo-fb-b.svg') ?>"></a>
                            </li>
                            <li>
                                <a href=""><img class="img-responsive fb " src="<?= $this->assetUrl('image/logo-tw-b.svg') ?>"></a>
                            </li>
                        </ul>
                        <ul class="list-inline  footer_menu">
                            <li><a href="<?= $this->url('default_home') ?>">Accueil</a></li>
                            <li><a href="<?= $this->url('party_search_party', ["page" => 1]) ?>">Se faire inviter</a></li>
                            <li><a href="<?= $this->url('addparty_add_party') ?>">Créer une soirée</a></li>
                            <li><a href="">Livre d'or</a></li>
                            <li><a href="">Contact</a></li>
                        </ul>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>