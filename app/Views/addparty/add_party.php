<!-- Formulaire d'ajout de soirée avec gestion des erreurs et récupération des entrées utilisateurs si le formulaire est incomplet. -->
<?php $this->layout('layout', ['title' => 'Ajout de soiree']) ?>

<?php $this->start('main_content') ?>    
   <?php if($form == false){ ?>
        <h3 class="hdp rose">Décrivez votre <span class="bleu">soirée pour attirer</span> les convives</h3><br>
        <form action="" method="POST">
           <fieldset class="register">
            <div class="form-group">
                <label for="type">Type de soirée</label>
                <input type="text" class="form-control" name="type_soiree" id="type" placeholder="Ex : soirée mexicaine, couscous, apéritif, ...">
                <?php if(isset($erreur['type_soiree'])){ echo $erreur['type_soiree']; } ?>
            </div>
            <div class="form-group">
                <label for="nb_perso">Nombre de personnes maximum</label>
                <input type="number" class="form-control" name="nombre_perso_max" id="nb_perso" value="1" min="1">
                <?php if(isset($erreur['nombre_perso_max'])){ echo $erreur['nombre_perso_max']; } ?>
            </div>
            <div class="form-group">
                <label for="adresse_soiree">Adresse</label>
                <input type="text" class="form-control" name="adresse_soiree" value="<?= $_SESSION['login']['adresse']; ?>" id="adresse_soiree" placeholder="Ex : 3 rue des Lilas">
                <?php if(isset($erreur['adresse_soiree'])){ echo $erreur['adresse_soiree']; } ?>
            </div>
            <div class="form-group">
                <label for="codePostale">Code postale</label>
                <input type="text" class="form-control" name="codePostale" value="<?= $_SESSION['login']['codePostal']; ?>" id="codePostale" placeholder="Ex : 75002">
                <?php if(isset($erreur['codePostale'])){ echo $erreur['codePostale']; } ?>
            </div>
            <div class="form-group">
                <label for="ville">Ville</label>
                <input type="text" class="form-control" name="ville_soiree" value="<?= $_SESSION['login']['ville']; ?>" id="ville" placeholder="Ex : PARIS">
                <?php if(isset($erreur['ville_soiree'])){ echo $erreur['ville_soiree']; } ?>
            </div>
            <div class="form-group">
                <label for="date_soiree">Date de la soirée</label>
                <input type="date" class="form-control" name="date_soiree" id="date_soiree" placeholder="YYYY-mm-dd">
                <?php if(isset($erreur['date_soiree'])){ echo $erreur['date_soiree']; } ?>
            </div>
            <div class="form-group">
                <label for="info">Information supplémentaire</label>
                <textarea class="form-control" name="info" id="info" cols="30" rows="10"></textarea>
            </div>
            <button type="submit" class="btn btn-info btn-block">Envoyer</button>
            </fieldset>
        </form><br><br>

       <?php }else{ ?><!-- Si la création est un succès -->
                    <div class="row success">
                        <div class="alert alert-success">
                            <strong>Bravo !</strong> <p>Votre soirée à bien été créer.<br> Espéront que vous trouverez chaussure à votre pieds en guise de convives.</p>
                            <a href="<?= $this->url("party_search_party", ["page" => 1]) ?>">Retour vers les soirées.</a>
                        </div>
                    </div>
           <?php } ?>
<?php $this->stop('main_content') ?>