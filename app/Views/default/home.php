<?php $this->layout('layout', ['title' => 'Accueil']) ?>

<?php $this->start('main_content') ?>
        <br>
        <p class="hdp">Les soirées déprimantes ?
            <br>c'est de l'histoire ancienne !</p>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-5 hidden-sm hidden-xs ">
                <!--         Se faire inviter dans une soirée          -->
                    <h2 class="incruste"><a href="<?= $this->url('party_search_party',["page" => 1]) ?>">Du genre à vous taper l'incruste ?</a></h2>
                    <p class="sous_titre1"><strong>Alors, on s'en fait plus Chloé ?!</strong></p>
                </div>
                <a href="<?= $this->url('party_search_party',["page" => 1]) ?>"><img class="img-responsive ancrei col-xs-offset-1 col-xs-3 hidden-md hidden-lg" src="<?= $this->assetUrl('image/ancre-incruste.svg') ?>"></a>

                <a href="#livre"><img class=" img-responsive hidden-md hidden-lg col-xs-3 livreor" src="<?= $this->assetUrl('image/ban.svg') ?>"></a>
                <span class="ou col-md-2 hidden-xs hidden-sm">ou</span>
                <!--         Crée une soirée          -->
                <div class=" col-md-5 hidden-sm hidden-xs ">
                    <h2 class="casserole"><a href="<?= $this->url('addparty_add_party') ?>">Du genre à passer à la casserole ?</a></h2>
                    <p class="sous_titre2"><strong>Et bien enfilez le tablier et faites nous rêver  !</strong></p>
                </div>
                <a href="<?= $this->url('addparty_add_party') ?>"><img class="ancrec img-responsive col-xs-3 hidden-md hidden-lg" src="<?= $this->assetUrl('image/ancre-casserole.svg') ?>"></a>

            </div>
        </div>
        <div class="row">
            <!-- cette div contient l'image gauche-->
            <div class="col-md-5 hidden-sm hidden-xs">
                <a href="<?= $this->url('party_search_party',["page" => 1]) ?>">
                    <br><img class="img-responsive lien" src="<?= $this->assetUrl('image/incrust.png') ?>">
                </a>
            </div>
            <!-- cette div contient l'image droite-->
            <div class="col-md-5 col-md-offset-2 hidden-sm hidden-xs">
                <br>
                <a href="<?= $this->url('addparty_add_party') ?>"><img class="img-responsive lien" src="<?= $this->assetUrl('image/casserole.png') ?>"></a>
            </div>
        </div>
        <br>
        <br class="hiden-sm hidden-md hidden-lg">
        <p class="hdp">Revivez vos soirées mémorables en image !</p>
        <!-- ici vous avez accès au livre d'or-->
        <a class="col-md-10 col-md-offset-1 hidden-sm hidden-xs" href=""><img class="livre" src="<?= $this->assetUrl('image/livre_or.png') ?>"></a>
<?php $this->stop('main_content') ?>
