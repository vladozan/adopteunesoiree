<?php $this->layout('layout', ['title' => 'Historique']) ?>

<?php $this->start('main_content') ?>  

<div class="row">
    <h3 class="hdp rose">Votre <span class="bleu"> historique </span> d'adoption </h3><br>          
    <ul class="nav nav-tabs nav-justified">
        <li class="<?php if($tab == "crees") { echo 'active'; } ?>"><a href="<?= $this->url("history_party", ["page" => 1, "tab" => "crees"]); ?>">Soirées crées</a></li>
        <li class="<?php if($tab == "adopt") { echo 'active'; } ?>"><a href="<?= $this->url("history_party", ["page" => 1, "tab" => "adopt"]); ?>">Soirées adoptées</a></li>
    </ul>
<?php if(isset($tab) AND $tab == "crees"){ ?>
    <div class="tab-pane">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Titre</th>
            <th>Personne max.</th>
            <th>Date</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
         <?php 
            foreach($data as $cree)
            { 
                if($cree["no_delete"] == 1)
                {
                     $tr_class = "alert-danger";
                }
                elseif($cree["no_delete"] == 0)
                {
                    $past = strtotime($cree["date_soiree"]) - time();
                    if($past <= 0)
                        $tr_class = "alert-success";
                    else
                        $tr_class = "";
                }
                if($cree["no_delete"] == 0 AND $past <= 0) $status = "vert fa fa-check";
                elseif($cree["no_delete"] == 0 AND $past > 0) $status = "fa fa-hourglass-half";
                else $status = "error  fa fa-times";

                ?>
              <tr class="<?= $tr_class; ?>">
                <td><a href="<?= $this->url("party_party", ["soiree" => $cree["id"]]) ?>" class="tableLink"><?= $cree["type_soiree"]; ?></a></td>
                <td><a href="<?= $this->url("party_party", ["soiree" => $cree["id"]]) ?>" class="tableLink"><?= $cree["nombre_perso_max"]; ?></a></td>
                <td><a href="<?= $this->url("party_party", ["soiree" => $cree["id"]]) ?>" class="tableLink"><?= date("d/m/y \à H:m:s" , strtotime($cree["date_soiree"])); ?></a></td>
                <td><a href="<?= $this->url("party_party", ["soiree" => $cree["id"]]) ?>" class="tableLink"><i class="<?= $status; ?>"></i></a></td>
              </tr> 
          <?php } ?>
        </tbody>
      </table>
            <?php
        // pagination
            $j = 1;
            $precedent = (isset($page))? $page -1 : 0;
            $suivant = (isset($page))? $page +1 : 1;
              ?>
            <ul class="pager">
                <?php if($precedent > 0) { ?><li><a href="<?= $this->url("history_party", ["page" => $precedent, "tab" => $tab]); ?>">Précédent</a></li><?php }
                if($page_total > 0){ ?>
                <select id="select_historique">
                <?php while($j <= $page_total){ ?>
                    <option class="col-md-1" <?php if($j == $page) echo "selected"; ?> value="<?= $j; ?>/crees"><?= $j; ?></option>
                <?php $j++; } ?>
                </select>
                <?php }
                if($suivant <= $page_total) { ?><li><a href="<?= $this->url("history_party", ["page" => $suivant, "tab" => $tab]); ?>">Suivant</a></li><?php } ?>
            </ul>
    </div>
    <?php }elseif(isset($tab) AND $tab == "adopt"){ ?>
    <div class="tab-pane">
         <table class="table table-hover">
        <thead>
          <tr>
            <th>Titre</th>
            <th>hote</th>
            <th>Demande</th>
            <th>Date</th>
            <th>Acceptation</th>
          </tr>
        </thead>
        <tbody>
         <?php 
            foreach($data as $adoptee)
            { 
                if($adoptee["validation"] == 0) $validation = "fa fa-hourglass-half";
                elseif($adoptee["validation"] == 1) $validation = "vert fa fa-check";
                else $validation = "error  fa fa-times";
                ?>
              <tr>
                <td><a href="<?= $this->url("party_party", ["soiree" => $adoptee["id_soiree"]]) ?>" class="tableLink"><?= $adoptee["type_soiree"] ?></a></td>
                <td><a href="<?= $this->url("party_party", ["soiree" => $adoptee["id_soiree"]]) ?>" class="tableLink"><?= $adoptee["pseudo"] ?></a></td>
                <td><a href="<?= $this->url("party_party", ["soiree" => $adoptee["id_soiree"]]) ?>" class="tableLink"><?= $adoptee["date"] ?></a></td>
                <td><a href="<?= $this->url("party_party", ["soiree" => $adoptee["id_soiree"]]) ?>" class="tableLink"><?= $adoptee["date_soiree"] ?></a></td>
                <td><a href="<?= $this->url("party_party", ["soiree" => $adoptee["id_soiree"]]) ?>" class="tableLink"><i class="<?= $validation ?>"></i></a></td>
              </tr> 
          <?php } ?>
        </tbody>
      </table>
        <?php
        // pagination
            $k = 1;
            $precedent = (isset($page))? $page -1 : 0;
            $suivant = (isset($page))? $page +1 : 1;
              ?>
            <ul class="pager">
                <?php if($precedent > 0) { ?><li><a href="<?= $this->url("history_party", ["page" => $precedent, "tab" => $tab]); ?>">Précédent</a></li><?php }
                if($page_total > 0){ ?>
                <select id="select_historique">
                <?php while($k <= $page_total){ ?>
                    <option class="col-md-1" <?php if($k == $page) echo "selected"; ?> value="<?= $k; ?>/adopt"><?= $k; ?></option>
                <?php $k++; } ?>
                </select><?php
                }
                if($suivant <= $page_total) { ?><li><a href="<?= $this->url("history_party", ["page" => $suivant, "tab" => $tab]); ?>">Suivant</a></li><?php } ?>
            </ul>
    </div>
    <?php } ?>
<!--           <script>alert($.urlParam('tab'));</script>-->
<!--            </div>-->
</div>
<?php $this->stop('main_content') ?>