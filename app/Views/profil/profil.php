<?php $this->layout('layout', ['title' => 'Profil']) ?>

<?php $this->start('main_content') ?>
<?php
foreach($data as $donnees){ ?>
    <div class="row">
        <div class="membre">
    <!--    Affichage de l'avatar   -->
            <div class="avatar col-md-offset-1 col-md-4">
                <img src="<?= $this->assetUrl($donnees['avatar']); ?>" class="img-responsive" alt="img-perso" width="200" height="200">
            </div>
    <!--    Affichage des informations du profil enregistré dans la base de données    -->
            <div class="informations col-md-7">
               <?php if (isset($_SESSION["login"]) AND $profil != $_SESSION['login']['id']){?><a href="<?= $this->url("profil_report", ["profil" => $profil]) ?>" >Signaler cette personne. <i class="fa fa-flag " aria-hidden="true"></i></a><?php }?>
                <h2><?= $donnees['nom']." ".$donnees['prenom']; ?>&nbsp;&nbsp;&nbsp;<?php if(isset($_SESSION["login"]) AND $_SESSION["login"]["id"] != $profil){ ?><a class="send-message" href="<?= $this->url("mailbox_newMessage", ["profil" => $profil]) ?>"><span class="glyphicon glyphicon-envelope"></span>Envoyez lui un message.</a><?php } ?></h2>
                <p>Pseudo : <?= $donnees["pseudo"]; ?></p>
                <p>Date de naissance : <?= $donnees["date_naissance"]; ?></p>
                <p>Profession : <?= $donnees["profession"]; ?></p>
                <p>Localisation : <?= $donnees["adresse"]." ". $donnees["code_postal"]." ". $donnees["ville"]; ?></p>
                <p>Email : <?= $donnees["mail"]; ?></p>
            </div>
        </div>
    </div>
    <hr>
    <!--    Affichage de la description du profil   -->
    <div class="row">
        <div class="description col-md-offset-2 col-md-8">
           <h3>Description</h3>
            <span class="guillemet-left fa fa-quote-left fa-2x"></span>
            <p class="description-text"><?= $donnees["description"]; ?></p>
            <span class="guillemet-right fa fa-quote-right fa-2x"></span>
        </div>
    </div>
    <!--    Affichage des commentaires laissés par les utilisateurs    -->
    <div class="row">
        <div class="commentaire col-md-offset-2 col-md-8">
          <h3>Commentaires</h3>
           <blockquote class="blockquote blockquote-reverse">
            <?php foreach($commentaire as $com){?>
            <p class=" commentaire-affiche"><?= $com["commentaires"]; ?></p>
            <footer class="blockquote-footer" ><a href="<?= $this->url("profil_profil", ["profil" => $com['id_posteur']]) ?>"><?= $com['pseudo_personne']?></a></footer><br>
            <?php } ?>
            </blockquote>
        </div>
    </div>
    <!--    Champs d'insertion des commentaires     -->
    <?php
    if(isset($_SESSION["login"]) AND $_SESSION["login"]["id"] != $profil){?>
    <div class="row commentaire">
       <form action="" method="POST">
            <div class="form-group avis col-md-12">
                <label for="comment">Laissez un avis</label>
                <textarea class="form-control" rows="5" name="commentaire" id="comment" placeholder="Laissez un commentaire"></textarea>
                <button type="submit" class="btn btn-primary btn-block">Envoyer</button>
            </div>
        </form>
    </div>
    <?php
   }
}
$this->stop('main_content') ?>
