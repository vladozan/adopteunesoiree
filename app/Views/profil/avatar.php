<?php $this->layout('layout', ['title' => 'Profil']) ?>

<?php $this->start('menu_setting') ?>
<div class="container header">
    <nav class="menu_setting">
        <span class="navbar-brand">Paramètre</span>
        <ul class="nav navbar-nav">
            <li><a href="<?= $this->url("setting_setting") ?>">Information Personnel</a></li>
            <li class="active-setting"><a href="<?= $this->url("setting_avatar") ?>">Avatar</a></li>
            <li><a href="">Notification</a></li>
            <li><a class="bleu" href="<?= $this->url("profil_profil", ["profil" => $_SESSION["login"]["id"]]) ?>">Voir mon profil</a></li>
        </ul>
    </nav>
</div>
<?php $this->stop('menu_setting') ?>
<?php $this->start('main_content') ?>

<!--    Upload de l'avatar du profil    -->
<img src="<?= $this->assetUrl($avatar["avatar"]) ?>" class="img-responsive" alt="" width="200" height="200">
    <form method="post" action="" enctype="multipart/form-data">
        <fieldset class="register">
           <div class="form-group">
            <label for="mon_fichier">Fichier (tous formats | max. 1 Mo) :</label><br />
                <input type="hidden" class="form-control" name="MAX_FILE_SIZE" value="200048576" />
                <input type="file" name="data" id="data" /><br />
            </div>
            <input type="submit" name="submit" value="Envoyer" />
        </fieldset>
    </form>
<?php $this->stop('main_content') ?>