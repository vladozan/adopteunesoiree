<?php $this->layout('layout', ['title' => 'Paramètre']) ?>
>
<?php $this->start('menu_setting') ?>
<div class="container header">
    <nav class="menu_setting">
        <span class="navbar-brand">Paramètre</span>
        <ul class="nav navbar-nav">
            <li class="active-setting"><a href="<?= $this->url("setting_setting") ?>">Information Personnel</a></li>
            <li><a href="<?= $this->url("setting_avatar") ?>">Avatar</a></li>
            <li><a href="">Notification</a></li>
            <li><a class="bleu" href="<?= $this->url("profil_profil", ["profil" => $_SESSION["login"]["id"]]) ?>">Voir mon profil</a></li>
        </ul>
    </nav>
</div>
<?php $this->stop('menu_setting') ?>

<?php $this->start('main_content');

if(isset($success) AND $success){ ?>
    <div class="alert alert-success">
      <strong>Modification réussites</strong><br>Vos modification on bien été pris en compte.
    </div>
<?php    
} ?>
<form action="" method="post">
   <fieldset class="register">
        <div class="form-group">
            <label for="">Nom</label>
            <input type="text" class="form-control" name="nom" value="<?php if(isset($info["nom"])) echo $info["nom"]; ?>">
            <?php if(isset($errors["nom"])) echo "<p class='error'>".$errors["nom"]."</p>"; ?>
        </div>
        <div class="form-group">
            <label for="">Prénom</label>
            <input type="text" class="form-control" name="prenom" value="<?php if(isset($info["prenom"])) echo $info["prenom"]; ?>">
            <?php if(isset($errors["prenom"])) echo "<p class='error'>".$errors["prenom"]."</p>"; ?>
        </div>
        <div class="form-group">
            <label for="">Pseudo</label>
            <input type="text" class="form-control" name="pseudo" value="<?php if(isset($info["pseudo"])) echo $info["pseudo"]; ?>">
            <?php if(isset($errors["pseudo"])) echo "<p class='error'>".$errors["pseudo"]."</p>"; ?>
        </div>
        <div class="form-group">
            <label for="">Date de naissance</label>
            <input type="date" class="form-control" name="date_naissance" value="<?php if(isset($info["date_naissance"])) echo $info["date_naissance"]; ?>">
            <?php if(isset($errors["date_naissance"])) echo "<p class='error'>".$errors["date_naissance"]."</p>"; ?>
        </div>
        <div class="form-group">
            <label for="">Profession</label>
            <input type="text" class="form-control" name="profession" value="<?php if(isset($info["profession"])) echo $info["profession"]; ?>">
            <?php if(isset($errors["profession"])) echo "<p class='error'>".$errors["profession"]."</p>"; ?>
        </div>
        <div class="form-group">
            <label for="">Adresse</label>
            <input type="text" class="form-control" name="adresse" value="<?php if(isset($info["adresse"])) echo $info["adresse"]; ?>">
            <?php if(isset($errors["adresse"])) echo "<p class='error'>".$errors["adresse"]."</p>"; ?>
        </div>
        <div class="form-group">
            <label for="">Code postal</label>
            <input type="text" class="form-control" name="code_postal" value="<?php if(isset($info["code_postal"])) echo $info["code_postal"]; ?>">
            <?php if(isset($errors["code_postal"])) echo "<p class='error'>".$errors["code_postal"]."</p>"; ?>
        </div>
        <div class="form-group">
            <label for="">Ville</label>
            <input type="text" class="form-control" name="ville" value="<?php if(isset($info["ville"])) echo $info["ville"]; ?>">
            <?php if(isset($errors["ville"])) echo "<p class='error'>".$errors["ville"]."</p>"; ?>
        </div>
        <div class="form-group">
            <label for="">Adresse Email</label>
            <input type="email" class="form-control" name="mail" value="<?php if(isset($info["mail"])) echo $info["mail"]; ?>">
            <?php if(isset($errors["mail"])) echo "<p class='error'>".$errors["mail"]."</p>"; ?>
        </div>
        <div class="form-group">
            <label for="">Description</label>
            <textarea name="description" class="form-control" cols="30" rows="10"><?php if(isset($info["description"])) echo $info["description"]; ?></textarea>
            <?php if(isset($errors["description"])) echo "<p class='error'>".$errors["description"]."</p>"; ?>
        </div>
    </fieldset><br>
    <div class="form-group">
        <input type="submit" class="btn btn-info form-control" value="Enregistrer">
    </div>
</form>
<?php $this->stop('main_content') ?>