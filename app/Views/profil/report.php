<?php $this->layout('layout', ['title' => 'Report']) ?>

<?php $this->start('main_content') ?>
<?php
if ($validee == false){
?>
<!--  Formulaire de signalisation de membre  -->
    <h3 class="hdp rose">Détaillez<span class="bleu"> nous votre</span> signalement</h3>
    <br>
    <form action="" method="post">
        <fieldset class="register">
            <div class="form-group">
                <label for="raison">Raison du signalement :</label>
                <select class="form-control" id="raison" name="raison">
                    <option value="Messages abusifs">Messages abusifs</option>
                    <option value="Personne malveillante">Personne malveillante</option>
                    <option value="Messages vulgaire">Messages vulgaire</option>
                    <option value="Autres">Autres</option>
                </select>
                <label for="autre">Veuillez détailler votre signalement :</label>
                <textarea name="contenu" class="form-control" cols="30" rows="10"></textarea>
                <?php if(isset($errors['contenu'])){echo $errors['contenu'];}?>
                    <button type="submit" class="button btn btn-primary btn-block">Envoyer</button>
            </div>
        </fieldset>
    </form>

<?php // Message de success
}else{ ?> 
    <div class="row success">
        <div class="alert alert-success">
            <strong>Rapport envoyé</strong>
            <p>Votre message a bien été envoyé.
                <br> Nous allons le traiter dans les plus bref délais.</p>
            <a href="<?= $this->url("profil_profil", ["profil" => $profil]) ?>">Retour vers le profil.</a>
        </div>
    </div>
<?php  }
$this->stop('main_content') ?>