<?php $this->layout('layout', ['title' => 'Recherche de soiree']) ?>

<?php $this->start('main_content') ?>
           <?php if(!isset($_SESSION["login"]) AND !$success){ ?>
            <div class="accueil container">
                <div class="row">
                    <!-- Formulaire d'inscription-->
                    <form class="col-sm-6 col-sm-offset-6 col-md-offset-7 col-md-5" method="POST" action="">
                        <fieldset class="register">
                            <div class="form-group">
                                <label class="bleu">Nom de Famille :</label>
                                <input class="form-control" type="text" value="<?php if(isset($secure['nom'])) echo $secure['nom']; ?>" name="nom" placeholder="Nom" />
                                <?php if(isset($errors["nom"])){ echo '<p class="error">'.$errors["nom"].'</p>';}?>
                            </div>

                            <div class="form-group">
                                <label class="rose">Prénom :</label>
                                <input class="form-control" type="text" value="<?php if(isset($secure['prenom'])) echo $secure['prenom']; ?>" name="prenom" placeholder="Prénom" />
                                <?php if(isset($errors["prenom"])){ echo '<p class="error">'.$errors["prenom"].'</p>';}?>
                            </div>
                            <div class="form-group">
                                <label class="bleu">Sexe :</label>
                                <select name="sexe" class="form-control">
                                    <option <?php if(isset($secure['sexe']) AND $secure['sexe'] == 0) echo "selected"; ?> value="0">Homme</option>
                                    <option <?php if(isset($secure['sexe']) AND $secure['sexe'] == 1) echo "selected"; ?> value="1">Femme</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="rose">Pseudonyme :</label>
                                <input class="form-control" type="text" value="<?php if(isset($secure['pseudo'])) echo $secure['pseudo']; ?>" name="pseudo" placeholder="Pseudo" />
                                <?php if(isset($errors["pseudo"])){ echo '<p class="error">'.$errors["pseudo"].'</p>';}?>
                            </div>

                            <div class="form-group">
                                <label class="bleu">Date de naissance :</label>
                                <input class="form-control" type="date" value="<?php if(isset($secure['date_naissance'])) echo $secure['date_naissance']; ?>" name="date_naissance" placeholder="YYYY-MM-DD" />
                                <?php if(isset($errors["date_naissance"])){ echo '<p class="error">'.$errors["date_naissance"].'</p>';}?>
                            </div>

                            <div class="form-group">
                                <label class="rose">Email :</label>
                                <input class="form-control" type="text" value="<?php if(isset($secure['mail'])) echo $secure['mail']; ?>" name="mail" placeholder="Mail" />
                                <?php if(isset($errors["mail"])){ echo '<p class="error">'.$errors["mail"].'</p>';}?>
                            </div>

                            <div class="form-group">
                                <label class="bleu">Adresse :</label>
                                <input class="form-control" type="text" value="<?php if(isset($secure['adresse'])) echo $secure['adresse']; ?>" name="adresse" placeholder="Adresse" />
                                <?php if(isset($errors["adresse"])){ echo '<p class="error">'.$errors["adresse"].'</p>';}?>
                            </div>

                            <div class="form-group">
                                <label class="rose">Code postal :</label>
                                <input class="form-control" type="text" value="<?php if(isset($secure['code_postal'])) echo $secure['code_postal']; ?>" name="code_postal" placeholder="Code Postal" />
                                <?php if(isset($errors["code_postal"])){ echo '<p class="error">'.$errors["code_postal"].'</p>';}?>
                            </div>

                            <div class="form-group">
                                <label class="bleu">Ville :</label>
                                <input class="form-control" type="text" value="<?php if(isset($secure['ville'])) echo $secure['ville']; ?>" name="ville" placeholder="Ville" />
                                <?php if(isset($errors["ville"])){ echo '<p class="error">'.$errors["ville"].'</p>';}?>
                            </div>

                            <div class="form-group">
                                <label class="rose">Profession :</label>
                                <input class="form-control" type="text" value="<?php if(isset($secure['profession'])) echo $secure['profession']; ?>" name="profession" placeholder="Profession" />
                                <?php if(isset($errors["profession"])){ echo '<p class="error">'.$errors["profession"].'</p>';}?>
                            </div>

                            <div class="form-group">
                                <label class="bleu">Mot de passe :</label>
                                <input class="form-control" type="password" name="password" placeholder="Mot de passe" />
                                <?php if(isset($errors["password"])){ echo '<p class="error">'.$errors["password"].'</p>';}?>
                            </div>

                            <div class="form-group">
                                <label class="rose">Confirmation de mot de passe :</label>
                                <input class="form-control" type="password" name="confirmpassword" placeholder="Confirmer Mot de passe" />
                                <?php if(isset($errors["confirmpassword"])){ echo '<p class="error">'.$errors["confirmpassword"].'</p>';}?>
                            </div>

                            <div class="form-group">
                                <label class="bleu">A propos de vous :</label>
                                <textarea class="form-control" name="description" value="<?php if(isset($secure['description'])) echo $secure['description']; ?>" placeholder="Parlez-nous de vous !" cols="20" rows="10"></textarea>
                                <?php if(isset($errors["description"])){ echo '<p class="error">'.$errors["description"].'</p>';}?>
                            </div>
                            <div class="checkbox">
                                <label class="rose">
                                    <input type="checkbox" name="cookie" value="1"><b>Connexion automatique</b>
                                    <p class="help-block">En cochant cette case vous acceptez l'utilisation des cookies.</p>
                                </label>
                            </div>
                            <input style="text-transform: uppercase; margin-bottom:10px;" class="form-control btn btn-default" name="register" type="submit" value="Envoyer" />
                        </fieldset>
                    </form>
                    <br>
                </div>
            </div> 
<!--            Succes d'inscription-->
            <?php }elseif(isset($_SESSION["login"]) AND isset($success) AND $success){ ?>
                <div class="accueil container">
                    <div class="row success">
                        <div class="alert alert-success">
                            <strong>Bravo !</strong>
                            <p>Votre inscription à bien été enregistré. Vous pouvez dès à présent trouver vos convives pour soirée solo.
                                <br> Ou alors vous pouvez toujours vous taper l'incruste.</p>
                            <a href="<?= $this->url("default_home") ?>">Retour vers l'accueil.</a>
                        </div>
                    </div>
                </div>
                <?php }
 $this->stop('main_content') ?>