<?php $this->layout('layout', ['title' => 'Confirmation mail']) ?>

<?php $this->start('main_content') ?>

<div class="accueil container">
    <div class="row success">
        <div class="alert alert-success">
            <strong>Bravo !</strong>
            <p>Votre Email à bien été confirmé. Vous pouvez maintenant accèdé livrement à notre site.</p>
            <a href="<?= $this->url("default_home") ?>">Retour vers l'accueil.</a>
        </div>
    </div>
</div>
<?php
 $this->stop('main_content') ?>