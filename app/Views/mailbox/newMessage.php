<?php $this->layout('layout', ['title' => 'Nouveau message']) ?>

<?php $this->start('main_content');
if(!$success)
{ ?>
<div class="row">
<h3 class="hdp rose">Nouveau <span class="bleu">message</span></h3><br>
 <form class="col-md-offset-2 col-md-8" action="" method="post">
        <div class="form-group">
            <label for="">Destinataire :</label>
            <p><?= $data["pseudo"] ?></p>
        </div>
        <div class="form-group">
            <label for="">Sujet :</label>
            <input class="form-control" type="text" name="titre" value="<?= isset($_POST["titre"]) ? $_POST["titre"] : "";  ?>">
            <?= isset($error["titre"])? "<div class='error'>".$error["titre"].'</div>' : ""; ?>
        </div>
        <div class="form-group">
          <label for="">Votre réponse :</label>
          <textarea class="form-control" name="message" rows="5" id=""><?= isset($_POST["message"]) ? $_POST["message"] : "";  ?></textarea>
          <?= isset($error["message"])? "<div class='error'>".$error["message"].'</div>' : ""; ?>
        </div>
        <div class="form-group">
            <input class="form-control btn btn-info" type="submit" value="Envoyer">
        </div>
        <input type="hidden" name="id_posteur" value="<?= $_SESSION["login"]["id"]; ?>">
        <input type="hidden" name="id_receveur" value="<?= $data["id"] ?>">
    </form>
</div>
<?php }else{ ?>
    <div class="row success">
        <div class="alert alert-success">
            <strong>Message envoyé</strong> <p>Votre message à bien été envoyé.<br></p>
            <a href="<?= $this->url("profil_profil", ["profil" => $data["id"]]) ?>">Retour au profil.</a>
        </div>
    </div> <?php
}

$this->stop('main_content') ?>
