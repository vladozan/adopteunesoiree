<?php $this->layout('layout', ['title' => 'Messagerie']) ?>

<?php $this->start('main_content') ?>  
  <h3 class="hdp rose">Avez <span class="bleu"> vous du </span> courrier ?</h3><br>          
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Pseudo</th>
        <th>Titre</th>
        <th>Date</th>
      </tr>
    </thead>
    <tbody>
     <?php 
        foreach($data as $donnees)
        { 
            if($donnees["views"] == 0) // switch de class si les messages sont lu ou pas.
                $tr_class = "alert-info";
            else
                $tr_class = "";
            ?>
            <tr class="<?= $tr_class ?>"><!--   Tableau qui affiche les messages      -->
            <td><a href="<?= $this->url("mailbox_message", ["message" =>$donnees["id"]]) ?>" class="tableLink"><?= $donnees["pseudo"]; ?></a></td>
            <td><a href="<?= $this->url("mailbox_message", ["message" =>$donnees["id"]]) ?>" class="tableLink"><?= $donnees["titre"]; ?></a></td>
            <td><a href="<?= $this->url("mailbox_message", ["message" =>$donnees["id"]]) ?>" class="tableLink"><?= date("d/m/y" , strtotime($donnees["date"])); ?></a></td>
          </tr> 
      <?php } ?>
    </tbody>
  </table>
<!--   Pagination -->
   <?php
    $j = 1;
    $precedent = (isset($page))? $page -1 : 0;
    $suivant = (isset($page))? $page +1 : 1;
      ?>
    <ul class="pager">
        <?php if($precedent > 0) { ?><li><a href="<?php echo $this->url("mailbox_mailbox", ["page" => $precedent]); ?>">Précédent</a></li><?php } 
        if($page_total > 0){ ?>
        <select id="select">
        <?php while($j <= $page_total){ ?>
            <option class="col-md-1" <?php if($j == $page) echo "selected"; ?> value="<?= $j; ?>"><?= $j; ?></option>
        <?php $j++; } ?>
        </select>
        <?php }
        if($suivant <= $page_total) { ?><li><a href="<?php echo $this->url("mailbox_mailbox", ["page" => $suivant]); ?>">Suivant</a></li><?php } ?>
    </ul>
<?php $this->stop('main_content') ?>  