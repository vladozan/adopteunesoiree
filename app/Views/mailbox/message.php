<?php $this->layout('layout', ['title' => 'Message']) ?>

<?php $this->start('main_content') ?>
<?php
    // affichage du message selectionné
    foreach($data as $mess2) { ?>
        <div class="row">
            <div class="informations col-md-offset-2 col-md-4">
                <h3>De : <a href="<?= $this->url("profil_profil", ["profil" => $mess2["id_membre"]]) ?>"><?= $mess2["pseudo"]; ?></a></h3>
                <p>Écrit le <?= date("d/m/y \à H:m:s" , strtotime($mess2["date"])); ?></p>
            </div><br>
            <div class="col-md-offset-2 col-md-3">
                <a href="<?= $this->url("mailbox_newMessage", ["profil" => $mess2["id_membre"]]) ?>" class="form-control btn btn-info">Répondre</a>
            </div>
        </div>
        <div class="row">
            <div class="messagerie col-md-offset-2 col-md-8">
                <h3><?= $mess2["titre"]; ?></h3>
                <div class="messagerie-text">
                    <?= $mess2["message"]; ?><br>
                </div>
            </div>
        </div>
<?php }
$this->stop('main_content') ?>
