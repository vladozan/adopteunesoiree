<?php
namespace Model;

use Model\My_Model;
use \W\Model\ConnectionModel;

class AddPartyModel extends My_Model
{
    public $dbh;
    
    public function __construct(){
        $this->dbh = ConnectionModel::getDbh();
    }
    
    public function insert($data){
        $sql = "INSERT INTO soiree (";
        foreach($data as $key => $value)
        {
            $sql .= $key.', ';
        }
        $sql .= 'id_hote) VALUES (';
        foreach($data as $key => $value)
        {
            $sql .= ':'.$key.', ';
            $array[$key] = $value;
        }
        $array["id_hote"] = 1;
        $sql .= ':id_hote)';
        $requete = $this->dbh->prepare($sql);
        $requete->execute($array);
    }
        
}