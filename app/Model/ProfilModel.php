<?php
namespace Model;

use Model\My_Model;
use \W\Model\ConnectionModel;

class ProfilModel extends My_Model
{        
    public $dbh;
    
    public function __construct(){
        $this->dbh = ConnectionModel::getDbh();
    }
    public function show_profil($profil){
        //Alors on affiche le profil du membre connecté par rapports a son id
        $reponse = $this->dbh->prepare('SELECT * FROM membre WHERE membre.id=:id');
        $reponse->execute(array(
            "id"                    =>$profil
        ));
        return $reponse->fetchAll();
    }
    public function show_commentaire($profil){
        
        $reponse = $this->dbh->prepare('SELECT * FROM commentaires WHERE id_personne=:id_perso ORDER BY id DESC');
        $reponse->execute(array(
            "id_perso"              => $profil
        ));  
        return $reponse->fetchAll();
    }
    public function insert_commentaire($secure, $profil){
        $requete = $this->dbh->prepare('INSERT INTO commentaires (commentaires, id_personne, pseudo_personne, id_posteur) VALUES (:commentaire, :id_personne, :pseudo, :id_posteur)');
        $requete->execute(array(
            "commentaire"           => $secure,
            "id_personne"           => $profil,
            "pseudo"                => htmlspecialchars($_SESSION["login"]['pseudo']),
            "id_posteur"            => htmlspecialchars($_SESSION["login"]['id'])
        ));
    }
    public function report($profil, $secure){
        $signalement = $this->dbh->prepare('INSERT INTO report (id_profil, id_signaleur, raison, contenu, date) VALUES (:id_profil, :id_signaleur, :raison, :contenu, NOW())');
        $signalement->execute(array(
            'id_profil'             =>htmlspecialchars($profil),
            'id_signaleur'          =>htmlspecialchars($_SESSION['login']['id']),
            'raison'                =>$secure['raison'],
            'contenu'               =>$secure['contenu']       
        ));
    }
            
}