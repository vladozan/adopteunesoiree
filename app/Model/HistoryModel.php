<?php
namespace Model;

use Model\My_Model;
use \W\Model\ConnectionModel;

class HistoryModel extends My_Model
{
    public $dbh;
    
    public function __construct(){
        $this->dbh = ConnectionModel::getDbh();
    }
    
    public function party_created($page){
        $where[] = "id_hote=:id_session";
        $where[] = array("id_session"=> htmlspecialchars($_SESSION["login"]["id"]));
        
        $limit = $this->count("soiree", $where, 13, $page, true);
        $crees = $this->dbh->prepare("SELECT * FROM soiree WHERE id_hote=:id_session ORDER BY date_soiree DESC LIMIT ".$limit.", 13");
        $crees->execute(array(
            "id_session" => htmlspecialchars($_SESSION["login"]["id"])
        ));
        return $crees->fetchAll();
    }
    public function invited_party($page){
        
        $where[] = "id_invite=:id_session";
        $where[] = array("id_session"=> htmlspecialchars($_SESSION["login"]["id"]));
        
        $limit = $this->count("requete_soiree", $where, 13, $page, true);
        $adoptees = $this->dbh->prepare("SELECT soiree.type_soiree, soiree.id AS id_soiree, soiree.date_soiree, membre.id AS id_membre, membre.pseudo, requete_soiree.* FROM requete_soiree 
        LEFT JOIN soiree ON requete_soiree.id_soiree=soiree.id 
        LEFT JOIN membre ON requete_soiree.id_hote=membre.id 
        WHERE id_invite=:id_session 
        ORDER BY date DESC LIMIT ".$limit.", 13");
        $adoptees->execute(array(
            "id_session" => htmlspecialchars($_SESSION["login"]["id"])
        ));
        return $adoptees->fetchAll();
    }
}