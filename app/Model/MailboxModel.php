<?php
namespace Model;

use Model\My_Model;
use \W\Model\ConnectionModel;

class MailboxModel extends My_Model
{
    public $dbh;

    public function __construct(){
        $this->dbh = ConnectionModel::getDbh();
    }

    public function mailbox($page){
        $where[] = "id_receveur=:id_session";
        $where[] = array("id_session" => htmlspecialchars($_SESSION["login"]["id"]));
        $limit = $this->count("discussion", $where, 13, $page, true);

        // Requete d'affichage des messages lu / non lu
        $messages = $this->dbh->prepare("SELECT membre.id AS id_membre, membre.pseudo, discussion.* FROM discussion
                                        LEFT JOIN membre ON discussion.id_posteur = membre.id
                                        WHERE id_receveur=:id_session
                                        ORDER BY date DESC LIMIT ".$limit.", 13");

        $messages->execute(array("id_session"        => htmlspecialchars($_SESSION["login"]["id"])));

        return $messages->fetchAll();
    }
    public function message($message){
        $messages = $this->dbh->prepare("SELECT membre.pseudo, membre.id AS id_membre, discussion.* FROM discussion
                                    LEFT JOIN membre ON discussion.id_posteur = membre.id
                                    WHERE discussion.id=:id AND discussion.id_receveur=:id_session");
        $messages->execute(array(
                                    "id"                => htmlspecialchars($message),
                                    "id_session"        => htmlspecialchars($_SESSION["login"]["id"])
        ));

        return $messages->fetchAll();
    }
    public function view_message($message)
    {
        //    mise à jour du message en tant que messahe " vue "
        $update = $this->dbh->prepare("UPDATE discussion SET views=1 WHERE id=:id AND id_receveur=:id_session");
        $update->execute(array(
            "id" => htmlspecialchars($message),
            "id_session" => htmlspecialchars($_SESSION["login"]["id"])
        ));
    }
    public function send_message($data)
    {
        $envoie = $this->dbh->prepare("INSERT INTO discussion (id_posteur, id_receveur, id_soiree, titre, message, date) VALUES(:id_posteur, :id_receveur, :id_soiree, :titre, :message, NOW())");
        $envoie->execute(array(
            "id_posteur"            => $data["id_posteur"],
            "id_receveur"           => $data["id_receveur"],
            "id_soiree"             => $data["id_soiree"],
            "titre"                 => $data["titre"],
            "message"               => $data["message"]
        ));
        return true;
    }
    public function getUser($id){
        $req = $this->dbh->prepare("SELECT pseudo, id FROM membre WHERE id = :id");
        $req->execute(array("id" => $id));
        return $req->fetch();
    }
}
