<?php
namespace Model;

use Model\My_Model;
use \W\Model\ConnectionModel;

class RegisterModel extends My_Model
{
    public $dbh;
    
    public function __construct(){
        $this->dbh = ConnectionModel::getDbh();
    }
    
    public function login($data)
    {
        $requete = $this->dbh->prepare('SELECT COUNT(*) as nb FROM membre WHERE pseudo=:pseudo AND password=:password');
        $requete->execute(array(
                            "pseudo"            => $data["pseudo"],
                            "password"          => hash('sha256', $data["password"])
        ));
        
        $d = $requete->fetch();
        return $d["nb"];
    }
    
    public function connect_user($data)
    {
        $requete = $this->dbh->prepare('SELECT * FROM membre WHERE pseudo=:pseudo AND password=:password');
        $requete->execute(array(
                            "pseudo"            => $data["pseudo"],
                            "password"          => hash('sha256', $data["password"])
        ));
        
        return $requete->fetch();
    }
    
    public function register($secure){
        
        $avatar_default = ($secure["sexe"] == 0)? "image/default_m.jpg" : "image/default_g.jpg";
        
        $requete = $this->dbh->prepare('INSERT INTO membre(nom, prenom, sexe, pseudo, date_naissance, mail, adresse, code_postal, ville, profession, password, description, avatar, token) VALUES(:nom, :prenom, :sexe, :pseudo, :date_naissance, :mail, :adresse, :code_postal, :ville, :profession, :password, :description, :avatar, :token)');
            $requete->execute(array(
                'nom'                   => $secure['nom'],
                'prenom'                => $secure['prenom'],
                'sexe'                  => $secure['sexe'],
                'pseudo'                => $secure['pseudo'],
                'date_naissance'        => $secure['date_naissance'],
                'mail'                  => $secure['mail'],
                'adresse'               => $secure['adresse'],
                'code_postal'           => $secure['code_postal'],
                'ville'                 => $secure['ville'],
                'profession'            => $secure['profession'],
                'password'              => hash('sha256', $secure['password']),
                'description'           => $secure['description'],
                'avatar'                => $avatar_default,
                'token'                 => $secure["token"]
            ));
            
            return $this->dbh->lastInsertId();
    }
    public function confirmAccount($token){
        $req = $this->dbh->prepare("UPDATE membre SET token_valid=:token WHERE token=:token");
        $req->execute(array("token" => $token,
                            "token_valid" => $token
                           ));
//        die(debug($req->execute()));
        return $req->rowCount();
    }
}