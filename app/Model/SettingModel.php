<?php
namespace Model;

use Model\My_Model;
use \W\Model\ConnectionModel;

class SettingModel extends My_Model
{
    public $dbh;

    public function __construct(){
        $this->dbh = ConnectionModel::getDbh();
    }

    public function select_info()
    {
        $req = $this->dbh->prepare("SELECT * FROM membre WHERE id=:id_session");
        $req->execute(array("id_session" => htmlspecialchars($_SESSION["login"]["id"])));
        
        return $req->fetch();
    }
    
    public function update_info($secure){
        $sql = 'UPDATE membre SET ';
        foreach($secure as $key => $value){
            $sql .= $key.' = :'.$key.', ';
            $array[$key] = $value;
        }
        $sql = substr($sql, 0, -2);
        $sql .= " WHERE id=:id_session";
        $array["id"] = htmlspecialchars($_SESSION["login"]["id"]);
        $req = $this->dbh->prepare($sql);
        $req->execute($array);
    }
    public function avatar(){
        $req = $this->dbh->prepare("SELECT avatar, id FROM membre WHERE id=:id_session");
        $req->execute(array('id_session' => $_SESSION['login']['id']));
        
        return $req->fetch();
    }
    public function change_avatar($path){
//          On sélectionne le nom de l'avatar précédent dans la base de donnée
        $delete = $this->dbh->prepare("SELECT avatar FROM membre WHERE id= :id_session");
        $delete->execute(array("id_session" => $_SESSION["login"]["id"]));

        while($avatar = $delete->fetch())
        {
            // Et on supprime l'avatar si le nom est différent de image/default_m.jpg et image/default_g.jpg ( qui sont les avatar par default)
            if($avatar["avatar"] != "image/default_m.jpg" AND $avatar["avatar"] != "image/default_g.jpg" OR $avatar["avatar"] != "")
            {
                unlink("assets/".$avatar["avatar"]);
            }            
        }
        // Mise à jour de l'avatar dans la base de donnée.
        $reponse = $this->dbh->prepare('UPDATE membre SET avatar =:avatar WHERE id =:id_session');
        $reponse->execute(array("avatar" => $path, "id_session" => htmlspecialchars($_SESSION["login"]["id"])));
    }
}