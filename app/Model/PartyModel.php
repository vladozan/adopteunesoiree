<?php
namespace Model;

use Model\My_Model;
use \W\Model\ConnectionModel;

class PartyModel extends My_Model
{
    public $dbh;
    
    public function __construct(){
        $this->dbh = ConnectionModel::getDbh();
    }
    public function search_party_select($getPage, $post=array())
    {
        $limit = $this->count("soiree", "date_soiree > NOW()", 10, $getPage, true);
        // requete d'affichage des soirées.
        if(isset($post["type_soiree"],
            $post["date_soiree"],
            $post["adresse_soiree"],
            $post["ville_soiree"],
            $post["codePostale"],
            $post["nombre_perso_max"])
            ){
            $array=array();
            $sql = "SELECT membre.id AS membre_id, membre.pseudo, membre.mail, soiree.* FROM soiree LEFT JOIN membre ON soiree.id_hote=membre.id WHERE ";
            foreach($post as $key => $value)
            {
                if(!empty($value))
                {
                    $sql.= htmlspecialchars($key)." LIKE :".htmlspecialchars($key)." AND ";	
				    $array[htmlspecialchars($key)] = "%".htmlspecialchars($value)."%";
                }
            }
//                $sql = substr($sql, 0, -4);
                $sql .= "date_soiree > NOW() LIMIT ".$limit.", 10";
                $req = $this->dbh->prepare($sql);
//                die(print_r($req));
                $req->execute($array);
        }else{
            $req = $this->dbh->query('SELECT membre.id AS membre_id, membre.pseudo, membre.mail, soiree.* FROM soiree LEFT JOIN membre ON soiree.id_hote=membre.id WHERE date_soiree > NOW() LIMIT '.$limit.', 10');
        }
        
        return $req->fetchAll();
    }
    
    public function party($id)
    {
        $req = $this->dbh->query('SELECT membre.id AS membre_id, membre.pseudo, membre.mail, soiree.* FROM soiree LEFT JOIN membre ON soiree.id_hote=membre.id WHERE soiree.id = '.$id);
        
        return $req->fetch();
    }
    public function validate_member($post, $id_soiree){
        
        foreach($post as $key => $value)
        {
            $validation = ($value == 0)? "1" : "2";
            $req = $this->dbh->prepare("UPDATE requete_soiree SET validation = {$validation} WHERE id_invite=:id_invite AND id_soiree=:id_soiree AND id_hote=:id_session");
            $req->execute(array("id_invite" => htmlspecialchars($key),
                                "id_soiree" => htmlspecialchars($id_soiree),
                                "id_session" => htmlspecialchars($_SESSION["login"]["id"])
                               ));
        }
    }
    
    public function show_validate_member($id_soiree, $bool){
        $status = ($bool)? "1" : "0";
        $req = $this->dbh->prepare('SELECT requete_soiree.*, membre.id as membre_id, membre.pseudo FROM requete_soiree LEFT JOIN membre ON requete_soiree.id_invite=membre.id WHERE id_soiree=:id_soiree AND id_hote=:id_session AND validation ='.$status);
        $req->execute(array(    
                                "id_session"        => htmlspecialchars($_SESSION["login"]["id"]),
                                "id_soiree"         => htmlspecialchars($id_soiree)
                            ));
        return $req->fetchAll();
    }
    
    public function has_ask($id_soiree, $id_invite){
        $findRequest = $this->dbh->prepare("SELECT * FROM requete_soiree WHERE id_soiree=:id_soiree AND id_invite=:id_invite");
        $findRequest->execute(array("id_soiree" => $id_soiree, "id_invite" => $id_invite));
        
        return $findRequest->fetch();
    }
    
    public function insert_request($id_invite, $id_hote, $id_soiree, $name_soiree, $message){
        
        // On créer une discussion entre l'hote et l'invite.
            $insertDiscussion = $this->dbh->prepare("INSERT INTO discussion (id_posteur, id_receveur, id_soiree, titre, message, date) VALUES (:id_posteur, :id_receveur,:id_soiree, :titre, :message, NOW())");
            $insertDiscussion->execute(array(
                "id_posteur"            => $id_invite,
                "id_receveur"           => $id_hote,
                "id_soiree"             => $id_soiree,
                "titre"                 => "Soirée : ".$name_soiree,
                "message"               => $message
            ));
            $lastId = $this->dbh->lastInsertId();
            
            // On créer une requete de soirée unique entre l'hote et l'invité
            $insertRequete = $this->dbh->prepare("INSERT INTO requete_soiree (id_hote, id_invite, id_soiree, date, views, validation, id_discussion) VALUES (:id_hote, :id_invite, :id_soiree, NOW(), :views, :validation, :id_discussion)");
            $insertRequete->execute(array(
                "id_hote"           => $id_hote,
                "id_invite"         => $id_invite,
                "id_soiree"         => $id_soiree,
                "views"             => 0,
                "validation"        => 0,
                "id_discussion"     => $lastId
            ));
    }
}