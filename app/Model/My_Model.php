<?php

namespace Model;

use W\Model\Model;
use \W\Model\ConnectionModel;

class My_Model extends Model
{
        public function __construct(){
            $this->dbh = ConnectionModel::getDbh();
        }
    /*
    * Fonction de pagination
    *@param $table STRING le nom de la table à utililser
    *@param $where STRING la close where de la requete
    *@param $limit_var INT le nombre de ligne minimum par page
    *@param $getPage INT la page actuel
    *@return $limit INT la ligne de départ dans le LIMIT
    */
    public function count($table, $where="1=1", $limit_var, $getPage=0, $pagination=false){
        if(is_array($where))
        {
            $count = $this->dbh->prepare('SELECT COUNT(*) as nb FROM '.$table.' WHERE '.$where[0]);
            $count->execute($where[1]);
        }else{
            $count = $this->dbh->query('SELECT COUNT(*) as nb FROM '.$table.' WHERE '.$where);
        }
        $data = $count->fetch();
        $nb = $data["nb"];
        $nb_page = ($nb <= $limit_var)? 0 : intval($nb / $limit_var);
        $nb_page_modulo = ($nb <= $limit_var)? 0 : $nb % $limit_var;
        $nb_page += ($nb_page_modulo > 0)?1:0;
        if(!$pagination) return $nb_page;
        
        $page = (isset($getPage))?$getPage:1;
        $limit = ($page < $nb_page)? ($page -1) * $limit_var : ($nb_page - 1) * $limit_var; 
        if($limit < 0) $limit = 0;    
        
        return $limit;
    }
    
    public function select($data, $table)
    {
        $sql = "SELECT * FROM {$table} WHERE ";

        foreach($data as $key => $value)
        {
            $sql .= $key."=:".$key." AND ";
        }
        
        $sql = substr($sql, 0, -4);
        
        $req = $this->dbh->prepare($sql);
        $req->execute($data);
        return $req->fetch();
    }
    public function requeteMessage()
    {
        $requete = $this->dbh->prepare("SELECT count(*) as nb FROM discussion WHERE id_receveur=:id_session AND views=0");
        $requete->execute(array(
            "id_session" => htmlspecialchars($_SESSION["login"]["id"])
        ));
        $data = $requete->fetch();
        return $data["nb"];
    }
}