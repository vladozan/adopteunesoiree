<?php

namespace Controller;

use W\Controller\Controller;
use Model\My_Model;
use Model\MailboxModel;
use W\App;

class My_Controller extends Controller
{
    public $nb;
    private $model;
    public function __construct(){

        $my_app = getapp();
        $route = $my_app->getCurrentRoute();
        $url_autorise = array("register_confirmAccount", "register_register", "register_login", "party_party", "party_search_party", "default_home", "default_index", "profil_profil", "mailbox_newMessage");
        if(!isset($_SESSION['login']) AND !in_array($route, $url_autorise)){
            $this->redirectToRoute('default_home');
        }
        $this->model = new My_Model();
        if(!isset($_SESSION["login"])){
            if(isset($_COOKIE["pseudo"], $_COOKIE["identifiant"]))
            {
                $this->cookie_connect();
            }
        }else{
            $uri = explode('/', $_SERVER["REQUEST_URI"]);
            $count = count($uri) - 2;

            if(isset($_SESSION["login"]) AND $uri[$count] == "message")
            {
                $count += 1;
                $modelMessage = new MailboxModel();
                $modelMessage->view_message($uri[$count]);
            }
            $_SESSION["messagerie"]["nb_message"] = $this->requeteMessage();
        }
    }
    public function secure($post){
        foreach($post as $key => $value){
            $secure[$key] = htmlspecialchars($value);
        }
        return $secure;
    }

    public function connect($data, $cookie=0){
        $this->session($data);
        if(isset($cookie) AND $cookie == "1"){
            setcookie("pseudo", $data["pseudo"], time() + 365*24*3600, '/', null, false, true);
            setcookie("identifiant", $data["password"], time() + 365*24*3600, '/', null, false, true);
        }
    }
    public function cookie_connect(){
        $cookie = $this->secure($_COOKIE);
        $cookie["password"] = $cookie["identifiant"];
        unset($cookie["identifiant"]);
        unset($cookie["PHPSESSID"]);
        $user = $this->model->select($cookie, "membre");
        $this->session($user);

    }
    private function session($data){

        $_SESSION["login"]["nom"]               = $data["nom"];
        $_SESSION["login"]["pseudo"]            = $data["pseudo"];
        $_SESSION["login"]["id"]                = $data["id"];
        $_SESSION["login"]["adresse"]           = $data["adresse"];
        $_SESSION["login"]["codePostal"]        = $data["code_postal"];
        $_SESSION["login"]["ville"]             = $data["ville"];
        $_SESSION["login"]["prenom"]            = $data["prenom"];
        $_SESSION["login"]["mail"]              = $data["mail"];
        $_SESSION["messagerie"]["nb_message"]   = "(".$this->requeteMessage().")";
    }
    public function requeteMessage(){
        $nb = $this->model->requeteMessage();
        return  $nb;
    }
    public function tokenGenerator()
    {
        $token = uniqid(rand());
        $_SESSION["login"]["token"] = $token;
        return $token;
    }
}
