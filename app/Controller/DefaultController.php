<?php

namespace Controller;

use Controller\My_Controller;

class DefaultController extends My_Controller
{

	/**
	 * Page d'accueil par défaut
	 */
	public function home()
	{
		$this->show('default/home');
	}
    public function index()
    {
        $this->show('default/home');
    }

}