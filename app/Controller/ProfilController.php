<?php
namespace Controller;

use Controller\My_Controller;
use \Model\ProfilModel;

class ProfilController extends My_Controller{

    public function profil($profil)
    {
        //Si $_GET['profil'] existe
        if (isset($profil)){
            $profil = htmlspecialchars($profil);

            $model = new ProfilModel();
            $data["data"] = $model->show_profil($profil);
            $data["commentaire"] = $model->show_commentaire($profil);
            $data["profil"] = $profil;
            //Si un utilisateur a envoyé un commentaire
            if(isset($_POST['commentaire']))
            {
                //Alors envoi du commentaire avec indication du pseudo et de l'id du posteur vers l'id de la personne qui recoit le commentaire 
                $secure = htmlspecialchars($_POST['commentaire']);
                $model->insert_commentaire($secure, $profil);
                //Redirection sur la page automatiquement après envoi du commentaire
                $this->redirectToRoute('profil_profil', ["profil" => $profil]);
            }
            $this->show("profil/profil", $data);
        }else{
            $this->redirectToRoute('profil_profil', ["profil" => $_SESSION["login"]["id"]]);
        }
       
    }
    public function report($profil){
         $array["validee"] = false;
         $array["profil"] = $profil;
        //Sécurisation des varirables POST
        if (isset($_POST['raison'], $_POST['contenu'])){
            $secure = $this->secure($_POST);
            //Vérification des champs
            if (strlen($secure['contenu']) < 2){
                $array["errors"]['contenu'] = "le contenu n'est pas correct.";
            }
            if(isset($errors)){
                $this->show("profil/report", $array);                
            }
            //Insertion dans la base de données
            if(!isset($errors)){
                $model = new ProfilModel();
                $model->report($profil, $secure);
                $array["validee"] = true;
            }
        }
        $this->show("profil/report", $array);
    }
   
}