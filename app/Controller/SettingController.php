<?php
namespace Controller;

use Controller\My_Controller;
use \Model\SettingModel;

class SettingController extends My_Controller{

    public function setting(){
        
        $model = new SettingModel();
        $data = $model->select_info();
        if(isset($_POST["nom"],
                 $_POST["prenom"],
                 $_POST["pseudo"],
                 $_POST["date_naissance"],
                 $_POST["profession"],
                 $_POST["adresse"],
                 $_POST["code_postal"],
                 $_POST["ville"],
                 $_POST["mail"],
                 $_POST["description"]))
        {
            $secure = $this->secure($_POST);
            if(strlen($secure['nom']) < 2 OR strlen($secure['nom']) > 50)
            {
                $errors["nom"] = "Le nom de Famille n'est pas correct. Il doit contenir entre 2 et 50 caractères.";
            }
            if(strlen($secure['prenom']) < 2 OR strlen($secure['prenom']) > 50)
            {
                $errors["prenom"] = "Le prénom n'est pas correct. Il doit contenir entre 2 et 50 caractères.";
            }

            if(strlen($secure['date_naissance']) < 2 OR strlen($secure['date_naissance']) > 10)
            {
                $errors["date_naissance"] = "La date n'est pas correcte. Elle doit contenir entre 2 et 10 caractères au format YYYY-MM-DD";
            }

            if(strlen($secure['mail']) < 6 OR strlen($secure['mail']) > 50)
            {
                $errors["mail"] = "L'adresse e-mail n'est pas correcte. Il doit contenir entre 6 et 50 caractères.";
            }

            if(strlen($secure['adresse']) < 2 OR strlen($secure['adresse']) > 50)
            {
                $errors["adresse"] = "L'adresse n'est pas correcte. Elle doit contenir entre 2 et 50 caractères.";
            }

            if(strlen($secure['code_postal']) != 5)
            {
                $errors["code_postal"] = "Le code postal n'est pas correct. Il doit contenir 5 chiffres.";
            }

            if(strlen($secure['ville']) < 3 OR strlen($secure['ville']) > 25)
            {
                $errors["ville"] = "La ville n'est pas correcte. Elle doit contenir entre 3 et 25 caractères.";
            }
            if(strlen($secure['profession']) > 50)
            {
                $errors["profession"] = "La profession n'est pas correcte. Il doit contenir moins de 50 caractères.";
            }      
            if(strlen($secure['description']) < 6 OR strlen($secure['description']) > 250)
            {
                $errors["description"] = "La description n'est pas pas correcte. Il doit contenir entre 6 et 250 caractères.";
            }
            
            if(!isset($errors)){
                $model->update_info($secure);
                $data = $model->select_info();
                $this->show("profil/setting", ["info" => $data, "success" => true]);
            }else{
                $this->show("profil/setting", ["info" => $data, "errors" => $errors]);
            }
            
        }else{           
            $this->show("profil/setting", ["info" => $data]);
        }
    }
    public function avatar(){
        $model = new SettingModel();
        if (isset($_FILES['data']))
        { 
            // Stockage des différentes erreurs d'upload
            $msgErreur = array(0 => "",
                              1 => "La taille du fichier est trop importante.",
                              2 => "La taille du fichier est trop importante.",
                              3 => "Erreur lors du transfert.",
                              4 => "Aucun fichier n'as été chargé.",
                              6 => "Dossier manquant.",
                              7 => "Echec d'écriture sur le disque.",
                              8 => "Une extension PHP à bloquée l'upload.",
                              "ext" => "L'extension est incorrecte.",
                              );
            
            $erreur = $msgErreur[$_FILES['data']['error']];
            // Listing des extension de fichier valide
            $extensions_valides = array( 'image/jpg' , 'image/jpeg' , 'image/gif' , 'image/png' );
            
            // On vérifie l'extension réelle du fichier pour éviter les script JS ou PHP transformé en format .jpg ( par exemple )
            $extension_upload = mime_content_type($_FILES['data']['tmp_name']);
            $extension = explode("/", $extension_upload);
            
            // Si l'extension est incorrect => message d'erreur
            if (!in_array($extension_upload,$extensions_valides) ) $erreur = $msgErreur["ext"];
//            Si le dossier existe et qu'il n'y a pas d'erreur on créer le nom du fichier avec son extension
            if (file_exists("assets/image/avatar_user") AND $erreur == "") {
                $path = "image/avatar_user/".time().".".$extension[1];
                $resultat = move_uploaded_file($_FILES['data']['tmp_name'], "assets/".$path);
                if (!$resultat) $erreur = $msgErreur[3];
            }
            
            // S'il n'y a pas d'erreur
            if ($erreur == "") 
            {
                $model->change_avatar($path);
            }
        }  
        $data = $model->avatar();
        $this->show("profil/avatar", ["avatar" => $data]);
    }
}