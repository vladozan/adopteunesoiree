<?php
namespace Controller;

use Controller\My_Controller;
use \Model\AddPartyModel;

class AddPartyController extends My_Controller{

    public function add_party()
    {
        $form = false;
        if(isset($_POST['type_soiree'], $_POST['nombre_perso_max'], $_POST['adresse_soiree'], $_POST['codePostale'], $_POST['ville_soiree'], $_POST['date_soiree'], $_POST['info']))
        {
            $secure = $this->secure($_POST);
            if(strlen($secure['type_soiree']) < 4 OR empty($secure['type_soiree']))
            {
                $erreur['type_soiree'] = "Le type de soirée a mal été remplis, veuillez mettre plus de 4 caractères";
            }
            if(strlen($secure['nombre_perso_max']) < 1 OR empty($secure['nombre_perso_max']))
            {
                $erreur['nombre_perso_max'] = "Le nombre de personnes à mal été remplis, veuillez indiquer au moins une personne";
            }
            if(strlen($secure['adresse_soiree']) < 5 OR empty($secure['adresse_soiree']))
            {
                $erreur['adresse_soiree'] = "La localisation a mal été remplis, veuillez mettre plus 5 caractères";
            }
            if(strlen($secure['codePostale']) != 5 AND !is_int($secure["codePostale"]))
            {
                $erreur['codePostale'] = "Le code postale a mal été remplis, veuillez inserer 5 caractères";
            }
            if(strlen($secure['ville_soiree']) < 3)
            {
                $erreur['ville_soiree'] = "La ville a mal été remplis, veuillez mettre au moins 3 caractères";
            }
            // Changement du format de la date.
            if(strlen($secure['date_soiree']) != 10)
            {
                if(preg_match('#^[0-9]{2}[/-]{1}[0-9]{2}[/-]{1}[0-9]{4}$#', $secure['date_soiree']))
                {
                    if(!preg_match('#^[0-9]{2}[/-]{1}[0-9]{2}[/-]{1}[0-9]{4}$#', $secure['date_soiree']))
                    {
                        $secure['date_soiree'] = preg_replace('#^([0-9]{2})[/-]{1}([0-9]{2})[/-]{1}([0-9]{4}$)#', '$3-$2-$1', $secure['date_soiree']);
                    }
                    else
                    {
                        $secure['date_soiree'] = preg_replace('#^([0-9]{4})[/-]{1}([0-9]{2})[/-]{1}([0-9]{2}$)#', '$1-$2-$3', $secure['date_soiree']);
                    }
                }else
                {
                    $erreur['date_soiree'] = "Le format date n'est pas bon";
                }
            }
            // S'il n'y a pas d'erreur
            if(!isset($erreur))
            {
                $partyModel = new AddPartyModel();
                $partyModel->insert($secure);
                // Insertion de la soirée
                $form = true;
            }
        }
        $array["form"] = $form;
        if(isset($erreur)) $array["erreur"] = $erreur;
        $this->show("addparty/add_party", $array);
    }
}