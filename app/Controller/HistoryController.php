<?php
namespace Controller;

use Controller\My_Controller;
use \Model\HistoryModel;

class HistoryController extends My_Controller{
    
    public function party($page, $tab){
        $model = new HistoryModel();
        $array["page"] = $page;
        $array["tab"] = $tab;
        if($tab == "crees"){
            $where[] = "id_hote=:id_session";
            $where[] = array("id_session"=> htmlspecialchars($_SESSION["login"]["id"]));
            $count = $model->count("soiree", $where, 13, true);
            $array["page_total"] = $count;
            $array["data"] = $model->party_created($page);
        }elseif($tab == "adopt"){
            $where[] = "id_invite=:id_session";
            $where[] = array("id_session"=> htmlspecialchars($_SESSION["login"]["id"]));
            $count = $model->count("requete_soiree", $where, 13, true);
            $array["page_total"] = $count;
            $array["data"] = $model->invited_party($page);
        }
        $this->show("history/history", $array);
    }
}