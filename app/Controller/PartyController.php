<?php
namespace Controller;

use Controller\My_Controller;
use \Model\PartyModel;

class PartyController extends My_Controller{
    
    public function search_party($page)
    {
        $model = new PartyModel();  

        $req = $model->search_party_select($page, $_POST);
        $count = $model->count("soiree", "date_soiree > NOW()", 10, true);
        $array["get_page"] = $page;
        $array["page_total"] = $count;        
        $array["requete"] = $req;
        $this->show("party/search_party", $array);
    }
    public function party($id)
    {
        $model = new PartyModel(); 
        if(isset($_POST["id_hote"],
          $_POST["id_invite"],
          $_POST["id_soiree"],
          $_POST["name_soiree"]))
        {
            if($_POST["id_hote"] == $_SESSION["login"]["id"]) $this->showNotFound();
            // sécurisation des variables
            $secure = $this->secure($_POST);
            // verification si l'utilisateur s'est déjà inscrit à la soirée ou pas
            $findRequest = $model->has_ask($secure["id_soiree"], $secure["id_invite"]);
            if(!$findRequest) // s'il n'est pas inscrit on envoie un message à l'hote de la soirée
            {
                // préparation du message à envoyé automatiquement à l'hote
                $message = "<a href='".$_SERVER["W_BASE"]."/profil/".htmlspecialchars($_SESSION['login']['id'])."'>".htmlspecialchars($_SESSION['login']['pseudo'])."</a> souhaiterait s'incruster à votre soirée \" <a href='".$_SERVER["W_BASE"]."/view_party/".$secure["id_soiree"]."'>".$secure["name_soiree"]."</a> \" ";
                $model->insert_request($secure["id_invite"], $secure["id_hote"], $secure["id_soiree"], $secure["name_soiree"], $message);

                // Message de succès
                $array["success"] = "Votre requête à bien été envoyé pour cette soirée."; 
            }
            else
            {
                // Message d'erreur
                $array["error"] = "Vous avez déjà envoyé une requête pour cette soirée. Soyez patient ou envoyer un message privée à notre hôte.";
            }
        }
        if(isset($_POST['validate'])){
            $model->validate_member($this->secure($_POST), $id);
        }
        if(isset($_POST["oui"]))
        {
            $model->validate_member($this->secure($_POST), $id);
        }
        $array["donnees"] = $model->party($id);
//        die(debug($array["data"]));
        if(isset($_SESSION["login"]) AND $array["donnees"]["id_hote"] == $_SESSION["login"]["id"]){
            $array["waiting"] = $model->show_validate_member($id, false);
            $array["validate"] = $model->show_validate_member($id, true);
        }
        
        $this->show("party/party", $array); 
    }
}