<?php
namespace Controller;

use Controller\My_Controller;
use \Model\MailboxModel;

class MailboxController extends My_Controller{


    public function mailbox($page)
    {
        $model = new MailboxModel();
        $array["data"] = $model->mailbox($page);
        $where[] = "id_receveur=:id_session";
        $where[] = array("id_session" => htmlspecialchars($_SESSION["login"]["id"]));
        $count = $model->count("discussion", $where, 13, true);
        $array["page_total"] = $count;
        $array["page"] = $page;
        $this->show("mailbox/mailbox", $array);
    }

    public function message($message){
        $model = new MailboxModel();
        $array["message"] = $message;
        if(isset($_POST["titre"], $_POST["message"], $_POST["id_receveur"], $_POST["id_posteur"], $_POST["id_soiree"])){
            $secure = $this->secure($_POST);
            $array["send"] = $model->send_message($secure);
            $this->show("mailbox/message", $array);
        }
        $array["data"] = $model->message($message);
        $this->show("mailbox/message", $array);
    }

    public function newMessage($id){
        $model = new MailboxModel();
        $error = "";
        $success = false;
        if(isset($_POST["titre"], $_POST["message"], $_POST["id_receveur"], $_POST["id_posteur"])){
            if(strlen($_POST["titre"]) < 2 OR strlen($_POST["titre"]) > 50){
                $error["titre"] = "Le titre doit contenir au minimum 2 caractères et maximum 50 caractères.";
            }
            if(strlen($_POST["message"]) < 10){
                $error["message"] = "Le message doit contenir au minimum 10 caractères";
            }
            if(!is_array($error)){
                $secure = $this->secure($_POST);
                $secure["id_soiree"] = "";
                $model->send_message($secure);
                $success = true;
            }
        }
        $data = $model->getUser($id);
        $this->show("mailbox/newMessage", ["data" => $data, "error" => $error, "success" => $success]);
    }
}
