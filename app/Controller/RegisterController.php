<?php
namespace Controller;

use Controller\My_Controller;
use Model\RegisterModel;
use vendor\phpmailer\phpmailer\PHPMailerAutoload;

class RegisterController extends My_Controller{
    
    public function login(){
        
        $secure = $this->secure($_POST);
        $registerModel = new RegisterModel();
        if($registerModel->login($secure) > 0)
        {
            $data = $registerModel->connect_user($secure);
            $this->connect($data, $_POST["cookie"]);
            $this->redirectToRoute('default_home');
        }else{
            $this->redirectToRoute('register_register');
        }
    }
    public function logout()
    {
        session_destroy();
        
        setcookie("pseudo", null, time() - 365*24*3600, '/');    
        setcookie("identifiant", null, time() - 365*24*3600, '/');  
        
        $this->redirectToRoute('register_register');
    }
    public function register(){
        if(isset($_SESSION["login"])){ $this->redirectToRoute("default_home"); }
            $success = false;
        
            if(isset($_POST['nom'],
           $_POST['prenom'],
           $_POST['pseudo'],
           $_POST['date_naissance'],
           $_POST['mail'],
           $_POST['adresse'],
           $_POST['code_postal'],
           $_POST['ville'],
           $_POST['profession'],
           $_POST['password'],
           $_POST['confirmpassword'],
           $_POST['description'])
          )
        {
            $secure = $this->secure($_POST);
            $secure["token"] = $this->tokenGenerator();
            if(strlen($secure['nom']) < 2 OR strlen($secure['nom']) > 50)
            {
                $errors["nom"] = "Le nom de Famille n'est pas correct.";
            }
            if(strlen($secure['prenom']) < 2 OR strlen($secure['prenom']) > 50)
            {
                $errors["prenom"] = "Le prénom n'est pas correct.<br>";
            }

            if(strlen($secure['date_naissance']) < 2 OR strlen($secure['date_naissance']) > 10)
            {
                $errors["date_naissance"] = "La date n'est pas correcte.<br>";
            }

            if(strlen($secure['mail']) < 6 OR strlen($secure['mail']) > 50)
            {
                $errors["mail"] = "L'adresse e-mail n'est pas correcte.<br>";
            }

            if(strlen($secure['adresse']) < 4 OR strlen($secure['adresse']) > 50)
            {
                $errors["adresse"] = "L'adresse n'est pas correcte.<br>";
            }

            if(strlen($secure['code_postal']) != 5)
            {
                $errors["code_postal"] = "Le code postal n'est pas correct.<br>";
            }

            if(strlen($secure['ville']) < 3 OR strlen($secure['ville']) > 25)
            {
                $errors["ville"] = "La ville n'est pas correcte.<br>";
            }
            if(strlen($secure['profession']) < 3 OR strlen($secure['profession']) > 50)
            {
                $errors["profession"] = "La profession n'est pas correcte.<br>";
            }

            if(strlen($secure['password']) < 3 OR strlen($secure['password']) > 70)
            {
                $errors["password"] = "Le mot de passe n'est pas correct.<br>";
            }

            if($secure['password'] != $secure['confirmpassword'] )
            {
                $errors["confirmpassword"] = "La confirmation du mot de passe n'est pas correcte.<br>";
            }        
            if(strlen($secure['description']) < 6 OR strlen($secure['description']) > 250)
            {
                $errors["description"] = "La description n'est pas pas correcte.<br>";
            }


            // Connexion PDO à la base de données
            if(!isset($errors))
            {                
                $model = new RegisterModel();
                $last_id = $model->register($secure);
                
                $_SESSION["login"]["nom"]               = $secure["nom"];
                $_SESSION["login"]["pseudo"]            = $secure["pseudo"];
                $_SESSION["login"]["id"]                = $last_id;
                $_SESSION["login"]["adresse"]           = $secure["adresse"];
                $_SESSION["login"]["codePostal"]        = $secure["code_postal"];
                $_SESSION["login"]["ville"]             = $secure["ville"];
                $_SESSION["login"]["prenom"]            = $secure["prenom"];
                $_SESSION["login"]["mail"]              = $secure["mail"];
                $_SESSION["login"]["token"]             = $secure["token"];

                if(isset($_POST["cookie"]) AND $_POST["cookie"] == "1")
                {
                    setcookie("pseudo", $secure["pseudo"], time() + 365*24*3600, null, null, false, true);
                    setcookie("identifiant", hash('sha256', $secure["password"]), time() + 365*24*3600, null, null, false, true);
                }            
                $success = true;
                
//                Affichage de l'inscription réussie
                 $header = array(
                'From: No reply',
                'Content-Type: text/html'
                );

                $subject = "Adopte une soiree.com : Comfirmation email";
                $message =  '<!DOCTYPE html>
                                <html lang="en">
                                    <head>
                                        <meta charset="UTF-8">
                                        <title>Confirmation email</title>
                                    </head>
                                    <body>
                                        <b><h1 style="color:#eb89af;">Adopte<span style="color:#84c1fa;"> une </span> soiree.com</h1></b>
                                        <p>Bravo et bienvenue sur notre site de partage culinaire.</p>
                                        <p>Passez au fourneau et épaté vos hôte !</p>
                                        <p>Mais avant ca, <a href="jinou-dev.fr/confirm/'.$secure['token'].'">cliquez ici pour confirmer votre adresse E-mail.</a></p>
                                        <p>A bientôt sur Adopte une soiree.</p>
                                        <p style="float: right;">@AdopteUneSoiree</p>
                                    </body>
                                </html>';

                $to = $_POST["mail"];
                mail($to,$subject,$message,implode("\r\n",$header));
            }
        }
        $this->show("register/register", ["success" => $success]);
    }
    public function confirmAccount($token){
        
        $model = new RegisterModel();
        $test = $model->confirmAccount($token);
        if($test == 0){ $this->show("register/confirm"); }
        else { $this->show("w_errors/404"); }
    }
    
}