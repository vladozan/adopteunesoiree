<?php

	$w_routes = array(
		['GET', '/', 'Default#index', 'default_index'],
		['GET', '/accueil/', 'Default#home', 'default_home'],
		['GET|POST', '/register/', 'Register#register', 'register_register'],
		['GET', '/confirm/[a:token]', 'Register#confirmAccount', 'register_confirmAccount'],
		['POST', '/login/', 'Register#login', 'register_login'],
		['GET', '/logout/', 'Register#logout', 'register_logout'],
		['GET|POST', '/party/[i:page]', 'Party#search_party', 'party_search_party'],
		['GET|POST', '/view_party/[i:soiree]', 'Party#party', 'party_party'],
		['GET|POST', '/addparty/', 'AddParty#add_party', 'addparty_add_party'],
		['GET|POST', '/profil/[i:profil]', 'Profil#profil', 'profil_profil'],
		['GET|POST', '/setting/', 'Setting#setting', 'setting_setting'],
		['GET|POST', '/avatar/', 'Setting#avatar', 'setting_avatar'],
		['GET|POST', '/report/[i:profil]', 'Profil#report', 'profil_report'],
		['GET|POST', '/mailbox/[i:page]', 'Mailbox#mailbox', 'mailbox_mailbox'],
		['GET|POST', '/newMesssage/[i:profil]', 'Mailbox#newMessage', 'mailbox_newMessage'],
		['GET|POST', '/message/[i:message]', 'Mailbox#message', 'mailbox_message'],
		['GET|POST', '/history/[i:page]/[crees|adopt:tab]', 'History#party', 'history_party'],
	);
